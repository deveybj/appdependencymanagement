@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('dep') ? ' has-error' : '' }}">
                            <label for="dep" class="col-md-4 control-label">Dependency:</label>

                            <div class="col-md-6">
                                <input id="dep" type="text" class="form-control" name="dep" value="{{ old('dep') }}" required autofocus>

                                @if ($errors->has('dep'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dep') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Search
                                </button>
                            </div>
                        </div>

                        <script>
                            curl -XPOST -u "ZKv9RUXwrMSrDhBUM4:bPhuTQYFcR75DersCSF2YtQXQyDhRwb3"
                            "https://bitbucket.org/site/oauth2/access_token \"
                            -d grant_type=authorization_code -d code={code}
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
