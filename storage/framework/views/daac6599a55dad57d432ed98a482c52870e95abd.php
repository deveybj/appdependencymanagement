<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <div class="form-group<?php echo e($errors->has('dep') ? ' has-error' : ''); ?>">
                            <label for="dep" class="col-md-4 control-label">Dependency:</label>

                            <div class="col-md-6">
                                <input id="dep" type="text" class="form-control" name="dep" value="<?php echo e(old('dep')); ?>" required autofocus>

                                <?php if($errors->has('dep')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('dep')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>